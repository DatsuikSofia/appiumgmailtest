package com.epam.listener;

import com.epam.connector.DriverManager;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;


public class CustomListener implements ITestListener {
    private static Logger LOG = Logger.getLogger(CustomListener.class.getName());

    @Override
    public void onTestStart(ITestResult iTestResult) {

    }

    @Override
    public void onTestSuccess(ITestResult iTestResult) {


    }

    @Override
    public void onTestFailure(ITestResult iTestResult) {
        LOG.info("TEST FAILURE");
        try {
            failed();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTestSkipped(ITestResult iTestResult) {

    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult iTestResult) {

    }

    @Override
    public void onStart(ITestContext iTestContext) {

    }

    @Override
    public void onFinish(ITestContext iTestContext) {
    }

    public static void failed() throws MalformedURLException {
        File scrFile = ((TakesScreenshot) DriverManager.getDriver()).getScreenshotAs(OutputType.FILE);
        try {
            FileUtils.copyFile(scrFile, new File("screenshots//screenshot.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}