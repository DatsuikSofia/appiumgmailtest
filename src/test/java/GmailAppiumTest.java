
import com.epam.connector.DriverManager;
import com.epam.listener.CustomListener;
import com.epam.pom.LoginPage;
import com.epam.util.JSONReader;
import com.epam.util.UserData;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.io.FileNotFoundException;
import java.net.MalformedURLException;
import java.net.URL;

@Listeners(CustomListener.class)
public class GmailAppiumTest {
    private static AndroidDriver<MobileElement> driver;
    private JSONReader jsonReader = new JSONReader();


    @Test(dataProvider = "user")
    public static void writeGmailMessage(UserData userData) throws MalformedURLException {
        LoginPage loginPage = new LoginPage();
        loginPage.goToGmailAccount();
        loginPage.addNewGmailAccount(userData.getUserEmail(), userData.getPassword());
        Assert.assertTrue(loginPage.verifyCorrectData(), "User add new correct account");
        loginPage.clickAcceptButton();

    }

    @DataProvider(parallel = true)
    public Object[] user() throws FileNotFoundException {
        return jsonReader.getData();
    }

}
